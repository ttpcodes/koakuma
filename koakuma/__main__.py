from koakuma.add_artist import add_artist as add_artist_func
from koakuma.upload import upload as upload_func

from argparse import ArgumentParser

parser = ArgumentParser()
subparsers = parser.add_subparsers()
add_artist = subparsers.add_parser(name='add-artist', description='Add a new artist.')
add_artist.set_defaults(func=add_artist_func)
add_artist.add_argument('artist')
upload = subparsers.add_parser(name='upload', description='Upload one or more images.')
upload.set_defaults(func=upload_func)
upload.add_argument('--extract', action='store_true')
upload.add_argument('source', nargs='+')

args = parser.parse_args()
args.func(args)
