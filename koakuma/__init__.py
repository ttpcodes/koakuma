from everett.component import ConfigOptions, RequiredConfigMixin
from everett.ext.yamlfile import ConfigYamlEnv
from everett.manager import ConfigManager

from base64 import b64encode


class Config(RequiredConfigMixin):
    required_config = ConfigOptions()
    required_config.add_option('booru', parser=str, doc='The base URL of the Szurubooru instance.')
    required_config.add_option('token', parser=str, doc='The token to authenticate to Szurubooru with.')
    required_config.add_option('username', parser=str, doc='The username to authenticate to Szurubooru with.')


config = ConfigManager(environments=[
    ConfigYamlEnv([
        'config.yaml'
    ])
])
config.with_options(Config())

token = '{}:{}'.format(config('username'), config('token'))
encoded_token = token.encode('utf-8')
AUTH_TOKEN = b64encode(encoded_token).decode('utf-8')

BOORU = config('booru')
