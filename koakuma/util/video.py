from youtube_dl import YoutubeDL

from io import BytesIO

# Needed to avoid shadowing of stdout.
import sys


def get_video(url):
    buffer = BytesIO()
    sys.stdout = buffer
    with YoutubeDL({
        'outtmpl': '-',
        'quiet': True
    }) as ydl:
        ydl.download([url])
    sys.stdout = sys.__stdout__

    return buffer.getbuffer()
