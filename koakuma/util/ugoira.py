from requests import get
from wand.image import Image

from io import BytesIO
from re import search
from zipfile import ZipFile


def get_ugoira(url):
    if match := search(r'https://www\.pixiv\.net/en/artworks/(\d+)$', url):
        query = match.group(1)
        json = get('https://www.pixiv.net/ajax/illust/{}/ugoira_meta'.format(query)).json()
        data = get(json['body']['originalSrc'], headers={
            'Referer': 'https://pixiv.net'
        }).content
        file = BytesIO(data)
        archive = ZipFile(file)

        with Image() as wand:
            for frame in json['body']['frames']:
                image = archive.read(frame['file'])
                with Image(blob=image) as img:
                    # The documentation says delay is in milliseconds but the delay is off by a factor of 1?
                    img.delay = frame['delay'] // 10
                    wand.sequence.append(img)
            # TODO: switch to APNG support once added to Imagemagick
            # For some reason, specifying the format as a parameter to make_blob() causes an error.
            wand.format = 'gif'
            return wand.make_blob()
